#include "ugv_control/unicycle_controller.hpp"
#include <cstdio>
#include <algorithm>

using namespace ugv_control;

//-----------------------------------------------------------------------------
UnicycleController::UnicycleController(UnicycleParameters params)
{
    params_=params;
}

//-----------------------------------------------------------------------------
UnicycleParameters UnicycleController::get_params()
{
    return params_; 
}

//-----------------------------------------------------------------------------
void UnicycleController::set_params(UnicycleParameters p)
{
    params_=p;
}

//-----------------------------------------------------------------------------
void UnicycleController::set_target_position(float x, float y)
{
    control_mode_ = Goto;
    target_pose_.x = x;
    target_pose_.y = y;
    target_pose_.theta = 0;
}

//-----------------------------------------------------------------------------
void UnicycleController::set_target_heading(float theta)
{
    control_mode_ = Rotate;
    target_pose_.x = 0;
    target_pose_.y = 0;
    target_pose_.theta = theta;
}

//-----------------------------------------------------------------------------
void UnicycleController::cancel_target()
{
    control_mode_ = Idle;
}

//-----------------------------------------------------------------------------
float UnicycleController::compute_velocity(Twist2D &cmd_vel, Pose2D pose)
{
    if(control_mode_==Goto)
    {
        // Angular and position error
        float dx = target_pose_.x - pose.x;
        float dy = target_pose_.y - pose.y;
        float dist = sqrt(dx * dx + dy * dy);
        float beta = atan2(dy, dx);
        float alpha = wrap_angle(beta - pose.theta);

        // Compute control velocity
        float vx = 0.0;
        float wz = 0.0;

        if (dist > params_.position_threshold)
        {
            if(std::fabs(alpha)<M_PI_4)
                vx=(params_.position_control_gain * dist)*std::cos(alpha)*std::cos(alpha);
            wz = params_.heading_control_gain * alpha;
        }
        else
        {
            control_mode_ = Idle;
        }

        cmd_vel.v = std::min(std::max(vx, - params_.max_linear_velocity) ,params_.max_linear_velocity);
        cmd_vel.w = std::min(std::max(wz, - params_.max_angular_velocity), params_.max_angular_velocity);
        return dist;
    }

    if(control_mode_==Rotate)
    {
        float wz=0;
        float err = wrap_angle(target_pose_.theta - pose.theta);

        if (std::fabs(err) > params_.heading_threshold)
        {
            wz = params_.heading_control_gain * err;
        }
        else
        {
            control_mode_ = Idle;
        }

        cmd_vel.v = 0.0f;
        cmd_vel.w = std::min(std::max(wz, - params_.max_angular_velocity), params_.max_angular_velocity);
        return err;
    }

    cmd_vel.v = 0.0f;
    cmd_vel.w = 0.0f;
    return 0.0f;
}

