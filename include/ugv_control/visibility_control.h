#ifndef UGV_CONTROL__VISIBILITY_CONTROL_H_
#define UGV_CONTROL__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define UGV_CONTROL_EXPORT __attribute__ ((dllexport))
    #define UGV_CONTROL_IMPORT __attribute__ ((dllimport))
  #else
    #define UGV_CONTROL_EXPORT __declspec(dllexport)
    #define UGV_CONTROL_IMPORT __declspec(dllimport)
  #endif
  #ifdef UGV_CONTROL_BUILDING_LIBRARY
    #define UGV_CONTROL_PUBLIC UGV_CONTROL_EXPORT
  #else
    #define UGV_CONTROL_PUBLIC UGV_CONTROL_IMPORT
  #endif
  #define UGV_CONTROL_PUBLIC_TYPE UGV_CONTROL_PUBLIC
  #define UGV_CONTROL_LOCAL
#else
  #define UGV_CONTROL_EXPORT __attribute__ ((visibility("default")))
  #define UGV_CONTROL_IMPORT
  #if __GNUC__ >= 4
    #define UGV_CONTROL_PUBLIC __attribute__ ((visibility("default")))
    #define UGV_CONTROL_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define UGV_CONTROL_PUBLIC
    #define UGV_CONTROL_LOCAL
  #endif
  #define UGV_CONTROL_PUBLIC_TYPE
#endif

#endif  // UGV_CONTROL__VISIBILITY_CONTROL_H_
