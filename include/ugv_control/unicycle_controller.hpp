#ifndef UGV_CONTROL__BASE_CONTROLLER__HPP
#define UGV_CONTROL__BASE_CONTROLLER__HPP

#include "common.hpp"

namespace ugv_control
{

struct UnicycleParameters
{
    float position_threshold;
    float position_control_gain;
    float heading_threshold;
    float heading_control_gain;
    float max_linear_velocity;
    float max_angular_velocity;
};

class UnicycleController
{
public:
    UnicycleController(UnicycleParameters params);
    UnicycleParameters get_params();
    void set_params(UnicycleParameters p);
    void set_target_position(float x, float y);
    void set_target_heading(float theta);
    void cancel_target();
    float compute_velocity(Twist2D &cmd_vel, Pose2D pose);
private:
    enum ControlMode {Idle, Goto, Rotate};
    ControlMode control_mode_;
    UnicycleParameters params_;
    Pose2D target_pose_;

};

} // namespace ugv_control

#endif