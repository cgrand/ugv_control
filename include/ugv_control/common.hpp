#ifndef UGV_CONTROL__COMMON__HPP
#define UGV_CONTROL__COMMON__HPP

#include <cmath>

namespace ugv_control
{

struct Pose2D
{
    float x;
    float y;
    float theta;
};

struct Twist2D
{
    float v;
    float w;
};

inline float wrap_angle(float angle)
{
    // convert angle to be in [-pi, pi]
    float signed_pi = std::copysign(M_PI, angle);
    // Set the value of difference to the appropriate signed value between pi and -pi.
    return std::fmod(angle + signed_pi,(2 * M_PI)) - signed_pi;
}

} // namespace ugv_control

#endif